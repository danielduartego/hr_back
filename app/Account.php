<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class Account extends Model
{
    public $timestamps = false;

    protected $fillable = [
        'id', 'company_name', 'phone', 'email', 'address', 'city_prov', 'country', 'postal_code', 'created_at', 'updated_at'
    ];
}


