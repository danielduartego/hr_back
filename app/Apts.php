<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class Apts extends Model
{

    protected $table = 'appointments';

    protected $fillable = [
        'id', 'account_id', 'name', 'points', 'active'
    ];
}


