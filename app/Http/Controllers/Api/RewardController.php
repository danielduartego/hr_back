<?php

namespace App\Http\Controllers\Api;

use Illuminate\Http\Request;
use App\Http\Controllers\Controller;
use App\Reward;

class RewardController extends Controller
{
    public function getRewards()
    {
        $rewards = Reward::where('account_id', auth()->user()->account_id)->get();

        foreach($rewards as $reward){
            $reward->active = ($reward->active == 1) ? true : false;
        }
        
        return $rewards;
    }

    public function store(Request $request)
    {
        $data = $request->validate([
            'name' => 'required|string',
            'value' => 'required|integer',
            'points' => 'required|integer',
            'active' => 'required',
        ]);

        $reward = Reward::create([
            'account_id' => auth()->user()->account_id,
            'name' => $request->name,
            'value' => $request->value,
            'points' => $request->points,
            'active' => $request->active
        ]);

        return response($reward, 201);
    }

    public function update(Request $request, Reward $reward)
    {
        if ($reward->account_id !== auth()->user()->account_id) {
            return response()->json('Unauthorized', 401);
        }

        $data = $request->validate([
            'name' => 'required|string',
            'value' => 'required|integer',
            'points' => 'required|integer',
            'active' => 'required',
        ]);

        $reward->update([
            'account_id' => auth()->user()->account_id,
            'name' => $request->name,
            'value' => $request->value,
            'points' => $request->points,
            'active' => $request->active
        ]);

        return response($reward, 200);
    }

    public function destroy(Reward $reward)
    {

        if ($reward->account_id !== auth()->user()->account_id) {
            return response()->json('Unauthorized', 401);
        }

        $reward->delete();

        return response()->json('Deleted reward item', 200);
    }
}
