<?php
namespace App\Http\Controllers\Api;

use App\Http\Controllers\Controller;
use App\User;
use Illuminate\Http\Request;
use Illuminate\Foundation\Auth\RegistersUsers;

class AuthController extends Controller
{
	use RegistersUsers;
	/**
	 * Handles Registration Request
	 *
	 * @param Request $request
	 * @return \Illuminate\Http\JsonResponse
	*/
    public function store(Request $request)
    {
		
		$request->validate([
			'name'     => 'required|min:3',
			'email'    => 'required|email|unique:users',
			'password' => 'required|min:6',
			'role'     => 'required|integer',
		]);
		
		$user = User::create([
			'account_id' => auth()->user()->account_id,
			'name'       => $request->name,
			'email'      => $request->email,
			'password'   => bcrypt($request->password),
			'role'       => $request->role,
			'active'     => ($request->active == true) ? 1 : 0,
        ]);
        
        if ($user) {
            $message = 'success';
        } else {
            $message = 'error';
        }

		return response()->json(['message' => $message], 200);
        
        // return toke on create only for test
		// $token = $user->createToken('userToken')->accessToken;
        // return response()->json(['token' => $token, 'user' => $user], 200);
		
	}

	/**
     * Handles Login Request
     *
     * @param Request $request
     * @return \Illuminate\Http\JsonResponse
    */
    public function login(Request $request)
    {

        $credentials = [
            'email' => $request->email,
            'password' => $request->password
        ];
 
        if (auth()->attempt($credentials)) {
            $token = auth()->user()->createToken('userToken')->accessToken;
            return response()->json(['access_token' => $token, 'user' => auth()->user()], 200);
        } else {
            return response()->json(['error' => 'UnAuthorised'], 401);
        }
    }

    public function logout()
    {

        auth()->user()->tokens->each(function ($token, $key) {
            $token->delete();
        });

        return response()->json('Logged out successfully', 200);
    }
 
    /**
     * Returns Authenticated User Details
     *
     * @return \Illuminate\Http\JsonResponse
     */
    public function details()
    {
        return response()->json(['user' => auth()->user()], 200);
    }

    /**
     * Returns Authenticated Users Details for one account
     *
     * @return \Illuminate\Http\JsonResponse
     */
    public function getUsers()
    {
        $users = User::where('account_id', auth()->user()->account_id)
        ->whereBetween('role', [1, 3])->get();

        foreach($users as $user){
            $user->active = ($user->active == 1) ? true : false;    
        }       
        return $users;
    }

    public function update(Request $request, User $user)
    {
        if ($user->account_id !== auth()->user()->account_id) {
            return response()->json('Unauthorized', 401);
        }

        $data = $request->validate([
            'name' => 'required|string',
            'email' => 'required|string',
            'role' => 'required|integer',
            'active' => 'required',
        ]);

        $user->update([
            'name' => $request->name,
            'email' => $request->email,
            'role' => $request->role,
            'active' => $request->active
        ]);

        $user->active = ($user->active == 1) ? true : false;

        return response($user, 200);
    }

    public function destroy(User $user)
    {

        if ($user->account_id !== auth()->user()->account_id) {
            return response()->json('Unauthorized', 401);
        }

        $user->delete();

        return response()->json('User Deleted', 200);
    }

}