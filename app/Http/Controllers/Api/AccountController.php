<?php

namespace App\Http\Controllers\Api;

use App\Http\Controllers\Controller;
use Illuminate\Http\Request;
use App\Account;
use App\Http\Resources\Account as AccountResource;

class AccountController extends Controller
{
    public function getAccount(){

        $account_id = auth()->user()->account_id;
        $account = Account::find($account_id);        
        return response()->json(['account' => $account], 200);
        
        // Using Resrouces
        //$account = Account::findOrFail(1);
        //return new AccountResource($account);
    }

    public function update(Request $request, Account $account)
    {

        $account = Account::find($request->id);   

        if ($account->id !== auth()->user()->account_id) {
            return response()->json('Unauthorized', 401);
        }

        $data = $request->validate([
            'company_name' => 'required|string',
            'phone' => 'required|string',
            'email' => 'required|email',
            'address' => 'required|string',
            'city_prov' => 'required|string',
            'country' => 'required|string',
            'postal_code' => 'required|string',
        ]);

        $account->update($data);

        return response(['account' => $account], 200);
    }
}
