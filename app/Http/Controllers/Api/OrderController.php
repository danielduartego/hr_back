<?php

namespace App\Http\Controllers\Api;

use Illuminate\Http\Request;
use App\Http\Controllers\Controller;
use App\Order;
use App\User;

class OrderController extends Controller
{
    // Get all Orders that belongs to specific account_id
    public function getOrders()
    {
        $orders = Order::where([['account_id', auth()->user()->account_id], ['status', 0]])->get();
        foreach ($orders as $order) {
            $order->user_name = User::find($order->user_id)->name;
        }

        return response($orders, 201);
    }

    // public function store(Request $request)
    // {
    //     $data = $request->validate([
    //         'name' => 'required|string',
    //         'value' => 'required|integer',
    //         'points' => 'required|integer',
    //         'active' => 'required',
    //     ]);

    //     $reward = Reward::create([
    //         'account_id' => auth()->user()->account_id,
    //         'name' => $request->name,
    //         'value' => $request->value,
    //         'points' => $request->points,
    //         'active' => $request->active
    //     ]);

    //     return response($reward, 201);
    // }

    public function update(Request $request, Order $order)
    {
        
        if ($order->account_id !== auth()->user()->account_id) {
            return response()->json('Unauthorized', 401);
        }

        $data = $request->validate([
            'id' => 'required|integer',
        ]);

        if ($request->action == 'close') {
            $order->update([
                'status' => 1, // closed
            ]);
        } else {
            $order->update([
                'status' => 2, // cancel 
                'reason' => $request->reason,
            ]);
        }
        

        return response($order, 200);
    }

    // public function destroy(Reward $reward)
    // {

    //     if ($reward->account_id !== auth()->user()->account_id) {
    //         return response()->json('Unauthorized', 401);
    //     }

    //     $reward->delete();

    //     return response()->json('Deleted reward item', 200);
    // }
}
