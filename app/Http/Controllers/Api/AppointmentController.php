<?php

namespace App\Http\Controllers\Api;

use Illuminate\Http\Request;
use App\Http\Controllers\Controller;
use App\Apts;

class AppointmentController extends Controller
{
    public function getApts()
    {
        $apts = Apts::where('account_id', auth()->user()->account_id)->get();

        foreach($apts as $apt){
            $apt->active = ($apt->active == 1) ? true : false;
        }
        
        return $apts;
    }

    public function store(Request $request, Apts $apt)
    {
        $data = $request->validate([
            'name' => 'required|string',
            'points' => 'required|integer',
            'active' => 'required',
        ]);

        $apt = Apts::create([
            'account_id' => auth()->user()->account_id,
            'name' => $request->name,
            'points' => $request->points,
            'active' => $request->active
        ]);

        return response($apt, 201);
    }

    public function update(Request $request, Apts $apt)
    {
        if ($apt->account_id !== auth()->user()->account_id) {
            return response()->json('Unauthorized', 401);
        }

        $data = $request->validate([
            'name' => 'required|string',
            'points' => 'required|integer',
            'active' => 'required',
        ]);

        $apt->update([
            'name' => $request->name,
            'points' => $request->points,
            'active' => $request->active
        ]);

        return response($apt, 200);
    }

    public function destroy(Apts $apt)
    {

        if ($apt->account_id !== auth()->user()->account_id) {
            return response()->json('Unauthorized', 401);
        }

        $apt->delete();

        return response()->json('Deleted apt item', 200);
    }
}
