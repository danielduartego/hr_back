<?php

use Illuminate\Http\Request;

/*
|--------------------------------------------------------------------------
| API Routes
|--------------------------------------------------------------------------
|
| Here is where you can register API routes for your application. These
| routes are loaded by the RouteServiceProvider within a group which
| is assigned the "api" middleware group. Enjoy building your API!
|
*/

Route::middleware('auth:api')->group(function () {
    //Logout
    Route::post('/logout', 'Api\AuthController@logout');

    //Users
    Route::get('/getUser', 'Api\AuthController@details');
    Route::get('/users', 'Api\AuthController@getUsers');
    Route::post('/user', 'Api\AuthController@store');
    Route::patch('/user/{user}', 'Api\AuthController@update');
    Route::delete('/user/{user}', 'Api\AuthController@destroy');

    //Account
    Route::get('/account', 'Api\AccountController@getAccount');
    Route::patch('/account/{id}', 'Api\AccountController@update');

    //Orders
    Route::get('/orders', 'Api\OrderController@getOrders');
    Route::patch('/order/{order}', 'Api\OrderController@update');

    //Rewards
    Route::get('/rewards', 'Api\RewardController@getRewards');
    Route::post('/rewards', 'Api\RewardController@store');
    Route::patch('/rewards/{reward}', 'Api\RewardController@update');
    Route::delete('/rewards/{reward}', 'Api\RewardController@destroy');

    //Appointments
    Route::get('/apts', 'Api\AppointmentController@getApts');
    Route::post('/apt', 'Api\AppointmentController@store');
    Route::patch('/apt/{apt}', 'Api\AppointmentController@update');
    Route::delete('/apt/{apt}', 'Api\AppointmentController@destroy');

});

// Route::post('/register','Api\AuthController@register');
Route::post('/login','Api\AuthController@login');

